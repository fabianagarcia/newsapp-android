package com.example.newsapp

fun main() {
    var baseDeDados = BaseDeDados()
    baseDeDados.upsert(Contato(1, "Bruno", "Mendonça", "9999", null))
    baseDeDados.upsert(Contato(2, "Fabi", "Etc", "9999", null))
    baseDeDados.upsert(Contato(3, "Nam", "Jun", "9999", null))
    baseDeDados.upsert(Contato(null, "teste", "nanana", "10", "asd"))
    baseDeDados.upsert(Contato(22, "teste", "nanana", "10", "asd"))
    baseDeDados.upsert(Contato(null, "teste", "nanana", "10", "asd"))

    baseDeDados.getContato(2).println()

    val bruno = baseDeDados.getContato(1)!!
    baseDeDados.upsert(Contato(bruno.id, bruno.primeiroNome, bruno.segundoNome, bruno.telefone, "bruno"))

    baseDeDados.deletarContato(3)

    baseDeDados.getContato(50).println()
}

fun Contato?.println() {
    println(this ?: "Contato não localizado")
}
package com.example.newsapp

data class Contato(
        val id: Int?,
        val primeiroNome: String,
        val segundoNome: String,
        val telefone: String,
        val apelido: String?)

class BaseDeDados() {
    private val contatos: MutableMap<Int, Contato> = mutableMapOf()

    fun upsert(contato: Contato) {
        val novoContato: Contato
        if(contato.id == null) {
            novoContato = Contato(
                    incrementarUltimoId(),
                    contato.primeiroNome,
                    contato.segundoNome,
                    contato.telefone,
                    contato.apelido
            )
        }
        else novoContato = contato
        contatos[contato.id ?: incrementarUltimoId()] = novoContato
    }

    private fun incrementarUltimoId(): Int {
        var ultimoId: Int = contatos.keys.max() ?: 0
        return ++ultimoId
    }

    fun getContatos(): List<Contato> = contatos.values.toList()

    fun getContatos(pesquisa: String): List<Contato> = contatos.values.filter { contato ->
        contato.primeiroNome.contains(pesquisa)
                || contato.segundoNome.contains(pesquisa)
                || contato.telefone.contains(pesquisa)
                || contato.apelido?.contains(pesquisa) ?: false
    }

    fun getContato(id: Int): Contato? {
        return contatos[id]
    }

    fun deletarContato(id: Int) {
        contatos.remove(id)
    }
}